@extends('layouts.master')

@section('content')

    <div class="container mt-3">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Nous contacter</h3>
            </div>
            <div class="card-body">
                <p>
                    Vous pouvez me contacter à l'adresse fabienduterte@mailfence.com
                </p>
            </div>
        </div>
    </div>

@endsection()
