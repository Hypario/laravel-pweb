<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ludothèque</title>
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
</head>
<body>
<header>
    @include('navbar')
</header>

<div class="container">

    @if (session()->has('success'))
        <p class="alert alert-success mt-3">{{ session()->get('success') }}</p>
    @elseif (session()->has('error'))
        <p class="alert alert-danger mt-3">{{ session()->get('error') }}</p>
    @endif

    @yield('content')

</div>

<footer class="navbar navbar-expand-lg navbar-dark bg-primary mt-5">
    <a class="navbar-brand" href="http://www.iut-lens.univ-artois.fr/">IUT de Lens</a>
</footer>

</body>
</html>
