@extends('layouts.master')

@section('content')

    <h1 class="text-center mt-3">Notre catalogue de jeu</h1>

    @if (Auth::check())
        <div class="text-center">
            <a href="{{ route('gallery.create') }}" class="btn btn-success ">Ajouter un jeu</a>
        </div>
    @endif

    <div class="row">
        @if(!$games->isEmpty())
            <div class="col-sm-8 mt-3">
                @foreach($games as $game)
                    <div class="card box-shadow">
                        <h3 class="card-header"><a href="{{ url('gallery', $game->id) }}">{{ $game->nom }}</a></h3>
                        <img class="card-img-top" src="{{ $game->thumbnail }}" alt="thumbnail">
                        <div class="card-body">
                            <p class="card-text">
                                {{ Str::limit($game->description, $limit = 150) }}
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <h2 class="text-center mt-3 text-secondary font-italic">La ludothèque est vide</h2>
    @endif

    <!-- tags widget -->
        <div class="col-sm-4 mt-3">
            <h5 class="card-header">Tags</h5>
            <ul class="list-group">
                @foreach($tags as $tag)
                    <a href="{{ route('tagged', $tag->id) }}"
                       class="list-group-item list-group-item-action">{{ $tag->label }}</a>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
