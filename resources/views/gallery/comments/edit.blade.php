@extends('layouts.master')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger mt-3" role="alert">
            <p>Des champs sont invalides</p>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="text-center mt-3">
        <h3>Modification de votre commentaire</h3>
        <hr class="mt-2 mb-2">
    </div>

    <form action="{{ route('comment.update', $comment->id) }}" method="POST">
        {{ @csrf_field() }}
        {{ @method_field('PUT') }}
        <div class="form-group">
            <label for="body">Commentaire : </label>
            <input type="text" class="form-control" name="body" placeholder="Trop bien ce jeu !" value="{{ $comment->body }}">
        </div>
        <button class="btn btn-success">Modifier</button>
    </form>

@endsection()
