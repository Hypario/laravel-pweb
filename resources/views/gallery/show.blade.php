@extends('layouts.master')

@section('content')

    @if (Auth::check())
        <div class="text-center">
            <div class="btn-group">
                <a href="{{ route('gallery.edit', $game->id) }}" class="btn btn-primary mt-3">
                    Modifier
                </a>

                <form action="{{ route('gallery.destroy', $game->id) }}" method="post" class="mt-3">
                    {!! csrf_field() !!}
                    {!! method_field('DELETE') !!}
                    <button type="submit" class="btn btn-danger">Supprimer</button>
                </form>
            </div>
        </div>
    @endif

    <div class="row mt-3 mb-3">
        <div class="col-md-6 mx-auto">
            <div class="card mb-12 box-shadow">
                <div class="card-header">
                    <h3 class="card-title">{{ $game->nom }}</h3>
                </div>
                <img class="card-img-top" src="{{ $game->thumbnail }}" alt="thumbnail">
                <div class="card-body">
                    <p class="card-text">{{ $game->description }}</p>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        âge minimal : {{ $game->age_min }}
                    </li>
                    <li class="list-group-item">
                        nombres de joueur : {{ $game->min_joueur }}-{{ $game->max_joueur }} joueur
                    </li>
                    <li class="list-group-item">
                        Temps de jeu moyen : {{ $game->min_duree }}-{{ $game->max_duree }}h
                    </li>
                    <li class="list-group-item">
                        Année de sortie : {{ $game->annee_sortie }}
                    </li>
                    @if (!$tags->isEmpty())
                        <li class="list-group-item">
                            @foreach($tags as $tag)
                                <span class="badge badge-info">
                                    {{ $tag->label }}
                                </span>
                            @endforeach
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>

    <h1 class="text-center mt-3 mb-3">Espace commentaire</h1>

    @foreach($comments as $comment)
        <div style="border-bottom: 1px dotted #ccc;" class="mb-3">
            <div class="media-body">
                <h4>{{ $comment->user->name }}

                    @if (Auth::id() == $comment->user->id)

                        <div class="btn-group">
                            <a href="{{ route('comment.edit', $comment->id) }}" class="btn btn-primary btn-sm">
                                Modifier
                            </a>

                            <form action="{{ route('comment.destroy', $comment->id) }}" method="post">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <button type="submit" class="btn btn-danger btn-sm">Supprimer</button>
                            </form>
                        </div>
                    @endif

                </h4>

                <p class="text-body">{{ $comment->body }}</p>
            </div>
        </div>
    @endforeach

    @if (Auth::check())
        <form action="{{ route('comment.store', $game->id) }}" method="POST" class="mb-3">
            {{ @csrf_field() }}
            {{ @method_field('PUT') }}
            <div class="form-group">
                <label for="body">Commentaire : </label>
                <input type="text" class="form-control" name="body" placeholder="Trop bien ce jeu !">
            </div>
            <button class="btn btn-success">Poster</button>
        </form>
    @endif
@endsection
