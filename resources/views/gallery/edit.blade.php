@extends('layouts.master')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger mt-3" role="alert">
            <p>Des champs sont invalides</p>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="text-center mt-3">
        <h3>Modification d'un jeu</h3>
        <hr class="mt-2 mb-2">
    </div>

    <form action="{{ route('gallery.update', $game->id) }}" method="POST">
        {!! csrf_field() !!}
        {!! method_field('PUT') !!}

        <div class="form-group">
            <label for="nom">Entrer le nom de votre jeu :</label>
            <input type="text" class="form-control" id="nom" name="nom" value="{{ $game->nom }}" placeholder="Pokémon">
        </div>

        <div class="form-group">
            <label for="urlimg">Url de l'image</label>
            <input type="text" class="form-control" id="urlimg" name="thumbnail" value="{{ $game->thumbnail }}" placeholder="https://picsum.photos/200/300">
        </div>

        <div class="form-group">
            <label for="annee">Entrer l'année de sortie :</label>
            <input type="text" class="form-control" id="annee" name="annee_sortie" value="{{ $game->annee_sortie }}"
                   placeholder="2019">
        </div>

        <div class="form-group">
            <label for="age">Entrer l'age minimum pour jouer à ce jeu :</label>
            <input type="text" class="form-control" id="age" name="age_min" value="{{ $game->age_min }}"
                   placeholder="11">
        </div>

        <div class="form-group">
            <label for="min_joueur">Entrer le nombre minimum de joueur possible :</label>
            <input type="text" class="form-control" id="min_joueur" name="min_joueur" value="{{ $game->min_joueur }}"
                   placeholder="1">
        </div>

        <div class="form-group">
            <label for="max_joueur">Entrer le nombre maximum de joueur possible :</label>
            <input type="text" class="form-control" id="max_joueur" name="max_joueur" value="{{ $game->max_joueur }}"
                   placeholder="3">
        </div>

        <div class="form-group">
            <label for="min_duree">Entrer le temps de jeu minimum possible :</label>
            <input type="text" class="form-control" id="min_duree" name="min_duree" value="{{ $game->min_duree }}"
                   placeholder="12">
        </div>

        <div class="form-group">
            <label for="max_duree">Entrer le temps de jeu maximum possible :</label>
            <input type="text" class="form-control" id="max_duree" name="max_duree" value="{{ $game->max_duree }}"
                   placeholder="24">
        </div>

        <div class="form-group">
            <label for="description">Description du jeu :</label>
            <textarea name="description" id="description" cols="30" rows="10" class="form-control"
                      placeholder="C'est 2 petit bonhommes qui se battent et...">{{ $game->description }}</textarea>
        </div>

        @foreach($tags as $tag)
            <div class="form-check-inline">
                @if (in_array($tag->id, $gametags))
                    <input type="checkbox" name="tags[]" value="{{ $tag->id }}" id="{{ $tag->label }}"
                           class="form-check-input" checked>
                @else
                    <input type="checkbox" name="tags[]" value="{{ $tag->id }}" id="{{ $tag->label }}"
                           class="form-check-input">
                @endif
                <label for="{{ $tag->label }}" class="form-check-label">{{ $tag->label }}</label>
            </div>
        @endforeach

        <div class="form-group">
            <button type="submit" class="btn btn-primary mb-3">Modifier</button>
        </div>
    </form>

@endsection()
