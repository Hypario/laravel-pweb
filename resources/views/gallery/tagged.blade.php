@extends('layouts.master')

@section('content')

    <h1 class="text-center mt-3">Jeux ayant le tag {{ $tag->label }}</h1>

    <div class="text-center">
        <a href="{{ route('gallery.create') }}" class="btn btn-success ">Ajouter un jeu</a>
    </div>

    <div class="row">
        <div class="col-sm-8 mb-3 mt-3">
            @if(!$games->isEmpty())
                @foreach($games as $game)
                    <div class="card mb-6 box-shadow">
                        <h3 class="card-header"><a href="{{ url('gallery', $game->id) }}">{{ $game->nom }}</a></h3>
                        <img class="card-img-top" src="{{ $game->thumbnail }}" alt="thumbnail">
                        <div class="card-body">
                            <p class="card-text">
                                {{ Str::limit($game->description, $limit = 150) }}
                            </p>
                        </div>
                    </div>
                @endforeach
            @else
                <h2 class="text-center mt-3 text-secondary font-italic">La ludothèque est vide</h2>
            @endif
        </div>

        <!-- tags widget -->
        <div class="col-sm-4 mt-3">
            <h5 class="card-header">Tags</h5>
            <ul class="list-group">
                @foreach($tags as $currentTag)
                    <a href="{{ route('tagged', $currentTag->id) }}"
                       class="list-group-item list-group-item-action @if ($currentTag->id == $tag->id) active @endif">{{ $currentTag->label }}</a>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
