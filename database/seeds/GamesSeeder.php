<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GamesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var \Faker\Generator $faker */
        $faker = $this->container->get(\Faker\Generator::class);

        for($i = 0; $i < 20; $i++) {
            DB::table('games')->insert([
                'thumbnail' => $faker->imageUrl(),
                'nom' => $faker->text(30),
                'annee_sortie' => $faker->date('Y'),
                'age_min' => $faker->numberBetween(10, 17),
                'min_joueur' => 1,
                'max_joueur' => $faker->numberBetween(2, 4),
                'min_duree' => 200,
                'max_duree' => 400,
                'description' => $faker->realText()
            ]);
        }

    }
}
