<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GalleryTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var \Faker\Generator $faker */
        $faker = $this->container->get(\Faker\Generator::class);

        for ($i = 0; $i < 20; $i++) {
            DB::table('games_tag')->insert([
                'games_id' => $faker->numberBetween(1, 20),
                'tag_id' => $faker->numberBetween(1, 20)
            ]);
        }
    }
}
