<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var \Faker\Generator $faker */
        $faker = $this->container->get(\Faker\Generator::class);

        for ($i = 0; $i < 20; $i++) {
            DB::table('tag')->insert([
                'label' => $faker->realText(10)
            ]);
        }
    }
}
