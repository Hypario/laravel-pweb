<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tag';

    public function games() {
        return $this->belongsToMany(Games::class);
    }
}
