<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Games extends Model
{

    protected $fillable = ['nom', 'annee_sortie', 'age_min', 'min_joueur', 'max_joueur', 'min_duree', 'max_duree', 'description'];

    protected $table = 'games';

    public function tag()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function comments()
    {
        return $this->hasMany(Commentaire::class);
    }

}
