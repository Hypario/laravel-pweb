<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{

    protected $fillable = ['body', 'games_id'];

    protected $table = 'commentaire';

    public function user() {
        return $this->belongsTo(User::class);
    }

}
