<?php

namespace App\Http\Controllers;

use App\Models\Commentaire;
use App\Models\Games;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class CommentaireController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws ValidationException
     */
    public function store(Request $request, $id)
    {
        // validate data
        $validated = $this->validate($request, [
            'body' => 'required',
        ]);

        if (Games::find($id)) {
            // create comment
            $comment = new Commentaire($validated);
            $comment->games_id = $id;
            $comment->user_id = Auth::id();

            $comment->save();

            // return to the game
            session()->flash('success', "Commentaire ajouté !");
            return redirect("/gallery/{$id}");
        }
        // we didn't find any comment
        session()->flash('error', "Ce commentaire n'existe pas");
        return redirect('/gallery');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = is_numeric($id) ? Commentaire::find($id) : null;
        if (!is_null($comment)) {
            return view('gallery.comments.edit', compact("comment"));
        }
        session()->flash('error', "Ce jeu n'existe pas.");
        return redirect('/gallery');
    }

    public function update(Request $request, $id)
    {
        /** @var Commentaire $comment */
        $comment = is_numeric($id) ? Commentaire::find($id) : null;

        if (!is_null($comment)) {
            $validated = $this->validate($request, [
                'body' => 'required',
            ]);

            $comment->update($validated);
            $comment->save();

            session()->flash('success', "Votre commentaire a bien été modifié.");
            return redirect("/gallery/$comment->games_id");
        }
        session()->flash('error', "Ce commentaire n'existe pas.");
        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        /** @var Commentaire $comment */
        $comment = is_numeric($id) ? Commentaire::find($id) : null;
        if (!is_null($comment)) {
            $comment->delete();
            session()->flash('success', "Votre commentaire a bien été supprimé.");
        } else {
            session()->flash('error', "Ce commentaire n'existe pas.");
        }
        return redirect()->back();
    }

}
