<?php

namespace App\Http\Controllers;

use App\Models\Games;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $games = Games::all();
        $tags = Tag::all();
        return view('gallery.index', compact('games', 'tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        return view('gallery.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'file' => 'required|image',
            'nom' => 'required',
            'annee_sortie' => 'required|integer',
            'age_min' => 'required|integer',
            'min_joueur' => 'required|integer',
            'max_joueur' => 'required|integer',
            'min_duree' => 'required|integer',
            'max_duree' => 'required|integer',
            'description' => 'required',
            'tags' => 'nullable|array'
        ]);

        $game = new Games($validated);

        // get filename
        $filename = $request->file->getClientOriginalName();
        // move it to the upload directory
        $request->file->move('upload', $filename);

        $game->thumbnail = "/upload/$filename";

        $game->save();

        $this->saveTag($game, $validated);

        session()->flash('success', "Ce jeu a bien été créée.");

        return redirect('/gallery');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /** @var Games $game */
        $game = is_numeric($id) ? Games::find($id) : null;
        if (!is_null($game)) {
            $tags = $game->tag()->get();
            $comments = $game->comments()->get();
            return view('gallery.show', compact('game', 'tags', 'comments'));
        }
        session()->flash('error', "Ce jeu n'existe pas.");
        return redirect('/gallery');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $game = is_numeric($id) ? Games::find($id) : null;
        $tags = Tag::all();
        if (!is_null($game)) {
            $gametags = $game->tag()->get()->map->id->toArray();
            return view('gallery.edit', compact("game", "tags", "gametags"));
        }
        session()->flash('error', "Ce jeu n'existe pas.");
        return redirect('/gallery');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        /** @var Games $game */
        $game = is_numeric($id) ? Games::find($id) : null;

        if (!is_null($game)) {
            $validated = $this->validate($request, [
                'thumbnail' => 'required|url',
                'nom' => 'required',
                'annee_sortie' => 'required|integer',
                'age_min' => 'required|integer',
                'min_joueur' => 'required|integer',
                'max_joueur' => 'required|integer',
                'min_duree' => 'required|integer',
                'max_duree' => 'required|integer',
                'description' => 'required',
                'tags' => 'nullable|array'
            ]);

            $game->update($validated);
            $game->save();

            $this->saveTag($game, $validated);

            session()->flash('success', "Ce jeu a bien été modifié.");

            return redirect("/gallery/$id");
        }
        session()->flash('error', "Ce jeu n'existe pas.");
        return redirect('/gallery');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        /** @var Games $game */
        $game = is_numeric($id) ? Games::find($id) : null;
        if (!is_null($game)) {
            $game->delete();
            session()->flash('success', "Le jeu a bien été supprimé.");
        } else {
            session()->flash('error', "Ce jeu n'existe pas.");
        }
        return redirect('/gallery');
    }

    public function tag($id) {

        $tag = is_numeric($id) ? Tag::find($id) : null;
        if (!is_null($tag)) {

            $games = $tag->games()->get();
            $tags = Tag::all();

            return view('gallery.tagged', compact('games', 'tag', 'tags'));
        }
        session()->flash('error', "Ce tag n'existe pas");
        return redirect('/gallery');
    }

    private function saveTag(Games $game, array $validated)
    {
        if (array_key_exists('tags', $validated) && !empty($validated['tags'])) {
            /** @var Model $game */
            $tags = Tag::all()->map->id->toArray();
            if (!empty(array_intersect($validated['tags'], $tags))) {
                $game->tag()->sync(array_intersect($validated['tags'], $tags));
            }
        } else {
            $game->tag()->sync([]);
        }
        $game->save();
    }
}
