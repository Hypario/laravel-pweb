<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{

    public function profil() {
        return view('logged');
    }

    public function about()
    {
        return view('home.about');
    }

    public function contact()
    {
        return view('home.contact');
    }
}
