<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// CRUD for gallery
Route::resource('gallery', 'GalleryController');

// the index of the website is the gallery
Route::get('/', function () { return redirect('/gallery'); });

// about and contact
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contact', 'HomeController@contact')->name('contact');

// route needed for comments
Route::put('/comment/{gallery}', 'CommentaireController@store')->name('comment.store');
Route::get('/comment/{comment}', "CommentaireController@edit")->name('comment.edit');
Route::put('/comment/{comment}/edit', 'CommentaireController@update')->name('comment.update');
Route::delete('/comment/{comment}', "CommentaireController@destroy")->name('comment.destroy');

// Authentication
Auth::routes();

// route to profile
Route::get('/profil', 'HomeController@profil')->name('profil')->middleware('auth');

// route to get games corresponding to tag;
Route::get('/tag/{tag}', 'GalleryController@tag')->name('tagged');
